import exp = require("constants");
import { readFileSync } from "fs";
import CanvasImage from "../CanvasImage";
describe("Create", () => {
    test("Create blank Image", () => {
        const image = CanvasImage.new(2, 2, "white");
        expect(image.width).toBe(2);
        expect(image.height).toBe(2);
        expect(image.colorData(0, 0, 2, 2)).toEqual([
            ...[255, 255, 255, 255],
            ...[255, 255, 255, 255],
            ...[255, 255, 255, 255],
            ...[255, 255, 255, 255]
        ]);
        const image2 = CanvasImage.new(2, 3, "blue");
        expect(image2.width).toBe(2);
        expect(image2.height).toBe(3);
        expect(image2.colorData(0, 0, 2, 3)).toEqual([
            ...[0, 0, 255, 255],
            ...[0, 0, 255, 255],
            ...[0, 0, 255, 255],
            ...[0, 0, 255, 255],
            ...[0, 0, 255, 255],
            ...[0, 0, 255, 255],
        ]);
    });

    test("Open Image", async () => {
        const src = readFileSync("src/test/test_image.png");

        const image = await CanvasImage.open("data:whatever/whatever;base64," + src.toString("base64"));
        // const image = await CanvasImage.open("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAMCAIAAADUCbv3AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAAUSURBVChTY/iPF4xKYwWUSP//DwCbIGaosFt0WQAAAABJRU5ErkJggg==");
        expect(image.width).toBe(10);
        expect(image.height).toBe(12);
    });
    test("Open Local Image", async () => {
        const src = readFileSync("src/test/test_image.png");

        const file = new File([src], "test_image.png");
        const image = await CanvasImage.openFromLocal(file);
        expect(image.width).toBe(10);
        expect(image.height).toBe(12);

    });
});
describe("Draw", () => {

    test("Draw Pixel", () => {
        const image = CanvasImage.new(10, 10);
        image.putPixel(0, 0, `white`);
        expect(image.colorData(0, 0, 1, 1)).toEqual([255, 255, 255, 255]);
        let expectedArray = Array(10 * 10 * 4).fill(0);
        // expectedArray.fill(0);
        expectedArray[0] = 255;
        expectedArray[1] = 255;
        expectedArray[2] = 255;
        expectedArray[3] = 255;
        expect(image.colorData(0, 0, 10, 10)).toEqual(expectedArray);
    });
    test("Draw Rectangle", () => {
        const image = CanvasImage.new(10, 10);
        image.fillRect(3, 2, 2, 2, "white");
        expect(image.colorData(3, 2, 2, 2)).toEqual([
            ...[255, 255, 255, 255],
            ...[255, 255, 255, 255],
            ...[255, 255, 255, 255],
            ...[255, 255, 255, 255]
        ])
        let expectedArray = Array(image.width * image.height * 4).fill(0);
        for (let x = 3; x < 3 + 2; x++) {
            for (let y = 2; y < 2 + 2; y++) {
                expectedArray[(y * image.width + x) * 4] = 255;
                expectedArray[(y * image.width + x) * 4 + 1] = 255;
                expectedArray[(y * image.width + x) * 4 + 2] = 255;
                expectedArray[(y * image.width + x) * 4 + 3] = 255;
            }
        }
        expect(image.colorData(0, 0, image.width, image.height)).toEqual(expectedArray);
    })
});
test("To DataUrl", () => {
    const image = CanvasImage.new(10, 12, "white");
    expect(image.dataURL()).toBe("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAMCAYAAABbayygAAAABmJLR0QA/wD/AP+gvaeTAAAAGElEQVQYlWP8////fwYiABMxikYVDmaFALe/BBSvzEzuAAAAAElFTkSuQmCC");
    expect(image.dataURL("image/jpeg")).toBe("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAAMAAoDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9U6KKKAP/2Q==");
    expect(image.dataURL("image/jpeg", 1)).toBe("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAMAAoDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD+/iiiigD/2Q==");
});
test("Crop", () => {
    const image = CanvasImage.new(10, 10, "white");
    let croppedImage = image.crop(5, 5, 2, 3);
    expect(croppedImage.width).toBe(2);
    expect(croppedImage.height).toBe(3);
    for (let x = 0; x < croppedImage.width; x++) {
        for (let y = 0; y < croppedImage.height; y++) {
            expect(croppedImage.getPixel(x, y)).toEqual([255, 255, 255, 255]);
        }
    }
    expect(croppedImage.getPixel(0, 0)).toEqual([255, 255, 255, 255]);
});
test("Resize", async () => {
    const image = CanvasImage.new(1, 1, "white");
    const resizedImage = await image.resize(5, 6);
    expect(resizedImage.width).toBe(5);
    expect(resizedImage.height).toBe(6);
    for (let x = 0; x < resizedImage.width; x++) {
        for (let y = 0; y < resizedImage.height; y++) {
            expect(resizedImage.getPixel(x, y)).toEqual([255, 255, 255, 255]);
        }
    }
});
test("Paste", () => {
    const whiteImage = CanvasImage.new(5, 5, "white");
    const blackImage = CanvasImage.new(5, 5, "black");
    const resultImage = CanvasImage.new(5, 10);
    resultImage.paste(whiteImage, 0, 0);
    resultImage.paste(blackImage, 0, 5);
    expect(resultImage.colorData(0, 0, 5, 5)).toEqual(whiteImage.colorData(0, 0, 5, 5));
    expect(resultImage.colorData(0, 5, 5, 5)).toEqual(blackImage.colorData(0, 0, 5, 5));
    
})
