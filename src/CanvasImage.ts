
class CanvasImage {
    readonly canvas: HTMLCanvasElement

    protected constructor() {
        this.canvas = document.createElement("canvas");
    }

    /**
     * Create a blank image
     * @param width width of this image
     * @param height height of this image
     * @param backgroundColor default color of this image
     * @returns CanvasImage instance
     */
    public static new(width: number, height: number, backgroundColor: string = "rgba(0, 0, 0, 0)") {
        const image = new CanvasImage();

        image.canvas.width = width;
        image.canvas.height = height;
        image.fillRect(0, 0, width, height, backgroundColor);
        return image;
    }

    /**
     * Open and return CanvasImage instance from src 
     * @param src Url or DataUrl of the image
     * @returns CanvasImage instance
     */
    public static async open(src: string) {
        const srcImg = new window.Image();
        await new Promise((resolve) => {
            srcImg.onload = resolve;
            srcImg.src = src;
        })
        const image = CanvasImage.new(srcImg.width, srcImg.height, "black");
        image.context.drawImage(srcImg, 0, 0);

        return image;
    }
    /**
     * Open and return CanvasImage instance from client's local fileObject
     * @param fileObject fileObject can be obtained from HTMLInputElement.files[index]
     * @returns CanvasImage instance
     */
    public static async openFromLocal(fileObject: File): Promise<CanvasImage> {

        const reader = new FileReader();

        await new Promise((resolve) => {
            reader.onload = resolve;
            reader.readAsDataURL(fileObject);
        })

        const dataURL = reader.result.toString();

        return this.open(dataURL)
    }
    private get context() {
        return this.canvas.getContext("2d");
    }
    get width() {
        return this.canvas.width;
    }
    get height() {
        return this.canvas.height;
    }
    putPixel(x: number, y: number, color: string) {
        this.fillRect(x, y, 1, 1, color);
    }
    fillRect(startX: number, startY: number, width: number, height: number, color: string) {
        this.context.fillStyle = color;
        this.context.fillRect(startX, startY, width, height);
    }
    /**
     * Returns the one-dimensional array containing the data in RGBA order, as integers in the range 0 to 255.
     */
    colorData(x: number, y: number, width: number, height: number) {
        return Array.from(this.context.getImageData(x, y, width, height).data);
    }
    
    dataURL(type: string = "image/png", quality: number = 0.92) {
        return this.canvas.toDataURL(type, quality)
    }
    /**
     * Return an array of numbers with length = 4 as RGBA value
     */
    getPixel(x: number, y: number) {
        return this.colorData(x, y, 1, 1);
    }
    /**
     * Return a cropped Image
     */
    crop(startX: number, startY: number, width: number, height: number) {
        const result = CanvasImage.new(width, height);
        result.context.putImageData(this.context.getImageData(startX, startY, width, height), 0, 0);
        return result;
    }
    /**
     * Returns a resized copy of this image.
     */
    async resize(width: number, height: number) {
        const result = CanvasImage.new(width, height);
        const srcImage = new Image();
        await new Promise((resolve) => {
            srcImage.onload = resolve;
            srcImage.src = this.dataURL();
        })
        result.context.drawImage(srcImage, 0, 0, width, height);
        return result;
    }
    /** 
     * Pastes another image into this image.
     */
    paste(srcImage: CanvasImage, topLeftX: number, topLeftY: number) {
        const srcData = srcImage.context.getImageData(0, 0, srcImage.width, srcImage.height);
        this.context.putImageData(srcData, topLeftX, topLeftY);
    }
}
export default CanvasImage;
